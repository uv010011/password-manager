package com.example.fyp;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.fyp.Interface;
import com.example.fyp.LogIn;
import com.example.fyp.R;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Signs out the user from their firebase account and exits the app
 */
public class Logout_Fragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returns  the  layout file
        return inflater.inflate(R.layout.logout_frag, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FirebaseAuth.getInstance().signOut(); //sign out the user from their firebase account
        System.exit(0); //exit the app
        getActivity().setTitle("Menu 1");

    }

}