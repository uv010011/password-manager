package com.example.fyp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * This class is simply used to display the help screen to the user. The user does not have to take any actions within this fragment
 */
public class help extends Fragment {

    /**
     * Inflates the help fragment in the current view
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.help, container, false); //inflate the help fragment in the layout
        return view;
        }

    /**
     * Saves the current state of this fragment once the view is created
      * @param view
     * @param savedInstanceState
     */
@Override
public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Help"); //title is set to help
        }

    /**
     * Once the view is created it states that the view will contain a menu and stores the current instance state
     * @param savedInstanceState
     */
    @Override
public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);

        }

    /**
     * Inflates the menu
     * @param menu
     * @param inflater
     */

@Override
public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    inflater.inflate(R.menu.main, menu); //inflate the menu
    menu.findItem(R.id.save).setVisible(false); //hide the save icon
    super.onCreateOptionsMenu(menu, inflater);
}
}