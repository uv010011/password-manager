package com.example.fyp;

import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Each social media logo is displayed as a clickable button so the class implements an onClickListener and a switch case
 * Once a button is clicked tha appropriate social media fragment gets displayed on the screen
 */
public class Passwords_Fragment extends Fragment implements View.OnClickListener {
    Button facebook,twitter,instagram,gmail,linkedin,snapchat,reddit,tiktok; //buttons to be presented

    /**
     * Inflates the current layout and gets the button Ids and sets an onClickListener to each one
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning the layout file
        View view = inflater.inflate(R.layout.activity_layout, container, false);

        facebook = view.findViewById(R.id.facebook_btn);
        facebook.setOnClickListener(this);

        twitter = view.findViewById(R.id.twitter_btn);
        twitter.setOnClickListener(this);

        instagram = view.findViewById(R.id.instagram_btn);
        instagram.setOnClickListener(this);

        gmail = view.findViewById(R.id.gmail_btn);
        gmail.setOnClickListener(this);

        linkedin = view.findViewById(R.id.linkedin_btn);
        linkedin.setOnClickListener(this);

        snapchat = view.findViewById(R.id.snapchat_btn);
        snapchat.setOnClickListener(this);

        reddit = view.findViewById(R.id.reddit_btn);
        reddit.setOnClickListener(this);

        tiktok = view.findViewById(R.id.tiktok_btn);
        tiktok.setOnClickListener(this);

        return view;
    }

    /**
     * Save the instance of the current view and change the activity title
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Passwords");//change title name to Passwords


    }

    /**
     * Save the current instance and inform the program that there is an options menu
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true); //inform existence of options menu
        super.onCreate(savedInstanceState);

    }

    /**
     * Inflates the menu layout and makes the save button invisible
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu); //inflate the menu layout
        menu.findItem(R.id.save).setVisible(false); //set to invisible
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Uses a switch case to check which social media button was clicked.
     * For each case the same process occurs. The current fragment is replaced with the fragment of the selected social media
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.facebook_btn:
                facebook facebook = new facebook(); //new instance of the facebook fragment
                FragmentTransaction transaction_f = getChildFragmentManager().beginTransaction(); //begin fragment transaction
                transaction_f.replace(R.id.display_view,facebook); //replace the current fragment with the facebook fragment
                transaction_f.commit();//commit the change
                break;
            case R.id.twitter_btn:
            twitter twitter = new twitter();
            FragmentTransaction transaction_t = getChildFragmentManager().beginTransaction();
            transaction_t.replace(R.id.display_view, twitter);
            transaction_t.commit();
            break;
            case R.id.instagram_btn:
                instagram instagram = new instagram();
                FragmentTransaction transaction_i= getChildFragmentManager().beginTransaction();
                transaction_i.replace(R.id.display_view, instagram);
                transaction_i.commit();
                break;
            case R.id.gmail_btn:
                gmail gmail = new gmail();
                FragmentTransaction transaction_g = getChildFragmentManager().beginTransaction();
                transaction_g.replace(R.id.display_view, gmail);
                transaction_g.commit();
                break;
            case R.id.linkedin_btn:
                linkedin linkedin = new linkedin();
                FragmentTransaction transaction_l = getChildFragmentManager().beginTransaction();
                transaction_l.replace(R.id.display_view, linkedin);
                transaction_l.commit();
                break;
            case R.id.snapchat_btn:
                snapchat snapchat = new snapchat();
                FragmentTransaction transaction_s = getChildFragmentManager().beginTransaction();
                transaction_s.replace(R.id.display_view, snapchat);
                transaction_s.commit();
                break;

            case R.id.tiktok_btn:
                tiktok tiktok = new tiktok();
                FragmentTransaction transaction_tt = getChildFragmentManager().beginTransaction();
                transaction_tt.replace(R.id.display_view, tiktok);
                transaction_tt.commit();
                break;
            case R.id.reddit_btn:
                reddit reddit = new reddit();
                FragmentTransaction transaction_r = getChildFragmentManager().beginTransaction();
                transaction_r.replace(R.id.display_view, reddit);
                transaction_r.commit();
                break;
        }


    }
}



