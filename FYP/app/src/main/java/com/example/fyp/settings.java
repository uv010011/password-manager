package com.example.fyp;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * The Settings class enables the user to alter their LastPass password and hint
 */
public class settings extends Fragment implements View.OnClickListener {
    EditText new_password, confirm, hint; //text boxes to be displayed
    String pass;
    Button update_password; //button to be displayed
    FirebaseAuth fa;
    FirebaseUser user; //firebase user
    String new_pass,confirmation; //strings used to store the entries


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /**
     * Inflates the layout of this fragment including text boxes and buttons
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.settings_fragment, container, false);
        new_password = view.findViewById(R.id.new_pass);
        confirm = view.findViewById(R.id.confirm_pass);
        hint = view.findViewById(R.id.new_hint);
        update_password = view.findViewById(R.id.update_pass_btn);
        update_password.setOnClickListener(this);
        user = FirebaseAuth.getInstance().getCurrentUser(); //get the current logged in user

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Settings");


    }

    /**
     * If the change button is pressed
     * @param v
     */
    @Override
    public void onClick(View v) {
        new_pass = new_password.getText().toString(); //get entered password
        confirmation = confirm.getText().toString();//get entered confirmed password
        //if the two passwords match,length is greater than or equal to 8 characters and the user is not null proceed
        if (new_pass.equals(confirmation)) {
            if (new_pass.length() >= 8) {
                if (user != null) {

                    user.updatePassword(new_password.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() { //update the users password
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getActivity(), "Password Changed Succesfully", Toast.LENGTH_LONG).show();//if task was successful display success email
                            } else {
                                Toast.makeText(getActivity(), "There was an error try again", Toast.LENGTH_LONG).show();//if task was unsuccessful display the error message
                            }

                        }

                    });
                }
                hint(); //update the hint

            } else {
                //if the password is less than 8 characters show an error message to the user
                new_password.setError("For security purposes the password must be at least 8 characters long Please try again");
                Toast.makeText(getActivity(), "For security purposes the password must be at least 8 characters long Please try again", Toast.LENGTH_LONG).show();
                }
            //if the passwords do not match show an error message to the user
        }else{ new_password.setError("The two passwords do not match. Please try again");
            Toast.makeText(getActivity(), "The two passwords do not match. Please try again", Toast.LENGTH_LONG).show();

        }
    }

    /**
     * Method checks if the hint is equal to the password and displays an error. Else it stores it in the hint file and deletes the old hint
     */
    public void hint() {
        String new_hint = hint.getText().toString(); //get the hint
        //if the hint matches the new password display an error message
        if (new_hint.contains(new_pass)) {
            hint.setError("For security reasons your password can't be your hint");
            Toast.makeText(getActivity(), "For security reasons your password can't be your hint", Toast.LENGTH_LONG).show();}
       //otherwise open the hint.txt file and store the new hint
        else{

            FileOutputStream fos = null;
            try {
                fos = getActivity().openFileOutput("hint.txt", Context.MODE_PRIVATE);//creates the new hint will be stored in
                fos.write(new_hint.getBytes()); //the hint is written in the file
                fos.close();// close the file once the writing is finished
                //in case of an input output error an error message gets displayed
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}