package com.example.fyp;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;



/**
 * This fragment generates a random and strong password for the user.
 * The password will contain a mixture of uppercase,lower case,numbers and special characters
 * The user should be able to copy said password and use it
 * Implements an OnClickListener for the two buttons
 */
public class password_generator_frag extends Fragment implements View.OnClickListener {
    String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}|;:',<.>/?"; //accepted characters to be used in the password
    int length = 12; //length of the password
    char[] password =new char[length];
    String result; // where the final result
    Button generator,copy_pass; //the buttons to be shown on the fragment
    TextView generated_pass; //text view that will display the random password
    Random random_generator = new Random(); //initialize the randomizer

    public password_generator_frag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_password_generator_frag, container, false); //inflate the current fragment
        //find the Ids of the buttons and text box. Each button is assigned to an onClickListener
        generated_pass = view.findViewById(R.id.generated_password);
        generator = view.findViewById(R.id.generate_btn);
        generator.setOnClickListener(this);
        copy_pass = view.findViewById(R.id.copy_btn);
        copy_pass.setOnClickListener(this);

        int itemID = 0;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Password Generator"); //name to be displayed on the top bar menu

    }

    /**
     * onClick method uses two switch cases to determine which button was clicked by the user and executes the appropriate code
     * @param v view of this fragment
     */
    public void onClick(View v) {
        switch (v.getId()){
            //if the user wants to generate a password
            case R.id.generate_btn:
                copy_pass.setText("COPY"); //reset text on copy button
                StringBuilder stringBuilder = new StringBuilder(length); //initialize string builder of size 12
                //while the number of characters is not greater than 12
                for (int counter =0; counter <= length; counter++) {
                    result = stringBuilder.append(characters.charAt(random_generator.nextInt(characters.length()))).toString(); //get a random character and add it to the result text
                }
                generated_pass.setText(result);//resulting password is displayed in the text box
                Toast.makeText(getActivity(),"Password was generated successfully",Toast.LENGTH_SHORT).show(); //informs the user about the password generation

                break;
                //if the user wants to copy the generated password
            case R.id.copy_btn:
                ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE); //gain access to clipboard on the user's phone
                ClipData clip = ClipData.newPlainText("Generated Password",result); //get the password
                clipboard.setPrimaryClip(clip); //set it as the primary clipboard text
                copy_pass.setText("COPIED!"); //change the string on the button to indicate that the text was copied
                Toast.makeText(getActivity(),"Password was copied!",Toast.LENGTH_SHORT).show();//informs the user about the password being copied

                break;

        }

    }

}

