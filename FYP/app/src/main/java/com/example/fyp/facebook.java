package com.example.fyp;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class facebook extends Fragment implements View.OnClickListener {
    /**
     * It receives the entered username and password text from the add_password_frag EditText fields.
     * Then by using an Advanced Encryption Standard algorithm the details get encrypted. Finally, once the save button gets pressed and
     * depending on the selected spinner item the encrypted text gets stored in the corresponding text file.
     *
     * If the user wishes to view their login details by clicking on the button the appropriate file is opened for reading and the
     * text gets decrytped and split into the username and password and gets displayed to the user's screen
     *
     * The user by using either of the copy buttons the corresponding text gets copied in their phone's clipboard. They can
     * the paste it in the text box as they are logging in.
     *
     * If the user wants to delete these details they can press the delete button and the text file containing those details will be removed
     * from their internal storage for security purposes.
     *
     * If the user wants to edit their username and password they can enter their new details in the two text boxes press the save button
     * and the new details will be encrypted and updated in the text file.
     */

    EditText username,password; //declaring the EditText item
    private static String AES = "AES"; //declaring and initialising the name of the AES algorithm
    private static final String KEY  ="aPdSgVkYp3s6v9y$B&E)H+MbQeThWmZq"; //declaring and initializing the value of the encryption key
    private static String FILE_NAME = null; //declaring and initializing the name of the text file to null
    String u_username,u_password; //declaring the String variables to be used for the encryption set-up
    private String data; //declaring where the final encrypted string will be stored
    Button decryption,copy_u,copy_p,delete; //declaring the buttons to be presented in this fragment
    /**
     * Creates the view of the fragment by inflating the proper layout and assigning values to the required variables
     * @param inflater gets the layout inflater
     * @param container gets the group of views
     * @param savedInstanceState gets the saved state of the UI
     * @return returns the new view of the UI
     */
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.facebook, container, false); //the view gets the layout of the add_password_frag that was inflated
        //gets the EditText items and the buttons. On the button an OnClickListener is set
        username = view.findViewById(R.id.add_username_f);
        password = view.findViewById(R.id.add_password_f);
        decryption = view.findViewById(R.id.decrypt_facebook);
        decryption.setOnClickListener(this);
        copy_u = view.findViewById(R.id.copy_username_f);
        copy_u.setOnClickListener(this);
        copy_p = view.findViewById(R.id.copy_password_f);
        copy_p.setOnClickListener(this);
        delete = view.findViewById(R.id.delete_facebook);
        delete.setOnClickListener(this);
        return view;
    }

    /**
     * Once the view gets created the top bar of the fragment gets adjusted in order to display the correct
     * title and icons. The buttons displayed in the passwords fragment are hidden as they otherwise will be displayed in the background
     * of this fragment
     * @param view the view of the fragment
     * @param savedInstanceState gets the saved state of the UI
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Facebook"); //set the title
        getActivity().findViewById(R.id.gmail_btn).setVisibility(View.INVISIBLE); //hide the gmail button
        getActivity().findViewById(R.id.facebook_btn).setVisibility(View.INVISIBLE); //hide the facebook button
        getActivity().findViewById(R.id.twitter_btn).setVisibility(View.INVISIBLE); //hide the twitter button
        getActivity().findViewById(R.id.instagram_btn).setVisibility(View.INVISIBLE); //hide the instagram button
        getActivity().findViewById(R.id.linkedin_btn).setVisibility(View.INVISIBLE); //hide the linkedin button
        getActivity().findViewById(R.id.snapchat_btn).setVisibility(View.INVISIBLE); //hide the snapchat button
        getActivity().findViewById(R.id.reddit_btn).setVisibility(View.INVISIBLE); //hide the reddit button
        getActivity().findViewById(R.id.tiktok_btn).setVisibility(View.INVISIBLE); //hide the tiktok button

    }

    /**
     * Once the fragment gets created it saves its current state.
     * Informs the program that it has an options menu on the side
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    /**
     * Inflates the side menu of the interface which is always displayed to the user
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main,menu); //menu inflater gets the identity of the menu to be inflated
        super.onCreateOptionsMenu(menu, inflater); //once the fragment is created it inflates the menu
    }

    /**
     * Checks to see if the save icon was pressed on the top bar and displays the correct value
     * @param item gets the menu item
     * @return returns boolean value depending on if it was successful or not
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId(); //gets the id of the selected menu item

        //checks if the id was the save icon
        if (id == R.id.save){
            //if it was then it tries to call the store method and shows a toast on screen to show that the data
            //were stored in the file
            try {
                store();
                Toast.makeText(getActivity(),"Saved!",Toast.LENGTH_SHORT).show();
                //in case of an error it catches the exception and displays the error
            } catch (Exception e) { e.printStackTrace(); }
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     * The method used to encrypt the entered strings using the AES algorithms. It combines the two inputs
     * then generates the encryption key and then it uses those values to create the encrypted string.
     * @return encrypted_data the encrypted string which will be stored
     * @throws Exception in case something goes wrong during the encryption
     */
    public String encryption () throws Exception {
        String for_encryption; //declare the variable where the combination of username and password will be stored together
        u_username = username.getText().toString(); //gets the username from the EditText box which is converted to string
        u_password  = password.getText().toString();//gets the password from the EditText box which is converted to string
        for_encryption = u_username + " " + u_password; //combine and store the two strings
        SecretKeySpec key = generateKey(); //get the encryption key by calling the generatekey() method
        Cipher cipher = Cipher.getInstance(AES); //gets the encryption cypher algorithm to be used. In this case AES
        cipher.init(Cipher.ENCRYPT_MODE, key); //initialise the cipher by setting it to encrypt mode and passing the encryption key
        byte[] encValue = cipher.doFinal(for_encryption.getBytes()); //encrypts the string and stores it in a byte array
        String encrypted_data = Base64.encodeToString(encValue, Base64.DEFAULT); //the final encrypted string is stored after it gets encoded to string
        return encrypted_data;
    }

    /**
     * This method generates the encryption key using the SHA-256 hashing and the given key.
     * @return returns the secretKey
     * @throws Exception in case something goes wrong during the generation of the encryption/decryption key
     */
    private SecretKeySpec generateKey () throws Exception {
        final MessageDigest digest = MessageDigest.getInstance("SHA-256"); //gets the message digest using the SHA-256
        byte[] bytes = KEY.getBytes("UTF-8"); //converts the key to bytes
        digest.update(bytes, 0, bytes.length); //the message digest gets the bytes of the key
        byte[] key = digest.digest(); //the digest key is generated and stored as bytes
        SecretKeySpec secretKey = new SecretKeySpec(key, "AES"); // the secret key is generated using the encryption key and the AES algorithm
        return secretKey;
    }

    /**
     * Gets the encrypted string from the text file and decrypts it using the generated decryption key
     * Also uses the AES encryption algorithm
     * @return returns the decrypted_string
     * @throws Exception in case something goes wrong during the decryption
     */
    private String decrypt() throws Exception {
        String encrypted_txt = load(); //calls load method to get the encrypted string
        SecretKeySpec key = generateKey(); //gets the decryption key
        Cipher c = Cipher.getInstance(AES); //the cipher to be used is set to AES
        c.init(Cipher.DECRYPT_MODE,key); //cipher is set to decrypt mode and is passed the decryption key
        byte[] decoded_value = Base64.decode(encrypted_txt,Base64.DEFAULT); //passes the encrypted string and gets the decoded value as a byte array
        byte[] final_decoded_val = c.doFinal(decoded_value); //finalises the decryption
        String decrypted_string = new String(final_decoded_val); //converts the byte array to string of the decrypted text
        return decrypted_string;

    }
    /**
     * The store method stores the encrypted text in the corresponding text file
     * @throws Exception in case something goes wrong while the data are being stored
     */
    public void store() throws Exception {
        data = encryption(); //calls the encryption method in order to encrypt the data and get the encrypted result
        FILE_NAME= "1.txt"; //name of the file
        FileOutputStream fos = null; //declare and initialise the fiel output stream
        try {
            fos = getActivity().openFileOutput(FILE_NAME, Context.MODE_PRIVATE);//creates the file in which the encrypted data will be stored in
            fos.write(data.getBytes()); //the encrypted data are written in the file
            fos.close();// close the file once the writing is finished
            //in case of an input output error an error message gets displayed
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The load method opens the text file and reads its contents
     * @return returns the read string read_str
     * @throws IOException
     */
    public String load() throws IOException {
        FileInputStream fis = getContext().openFileInput("1.txt"); //open the text file for read
        InputStreamReader isr = new InputStreamReader(fis); //declare and initialise the input stream reader
        BufferedReader br = new BufferedReader(isr); //declare and initialise the buffered reader
        StringBuilder sb = new StringBuilder(); //declare and initialise the string builder
        String read_str; //declare the string variable in which the read text will be stored

        //while the current line is not empty copy the data using the string builder
        //if the current line is null which means the EOF has been reached so it stops reading
        while ((read_str = br.readLine()) != null) {
            sb.append(read_str);
        }
        read_str = sb.toString();//convert the string builder contents to string
        return read_str;

    }

    /**
     * The method uses a switch case to check which button was clicked in order to
     * execute the correct sequence. In total there are 4 buttons
     * @param v the view of the fragment
     */
    @Override
    public void onClick(View v) {
        String set = null; //will be used to get the decrypted text
        String[] split = null; //will be used to store the split string. In position 0 it will be the username and in position 0 it will be the password
        //the switch case will use the Ids of the buttons to figure out which one was selected
        switch (v.getId()) {

            //in case the user wants to view their login details for facebook
            //it decrypts the text, splits it and displays it to the user
            case R.id.decrypt_facebook:
                try {
                    set = decrypt(); //gets the decrypted text
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                split = set.split("\\s+"); //split the text when there's a space which is used to separate the username from the password
                username.setText(split[0], TextView.BufferType.EDITABLE); //set the value of the username text box to the decrypted username value
                password.setText(split[1], TextView.BufferType.EDITABLE);//set the value of the password text box to the decrypted password value
                Toast.makeText(getActivity(),"Your Facebook login details",Toast.LENGTH_LONG).show(); //toast message is displayed on the screen
                break;

             //in case the user wants to copy their username the appropriate sequence is executed
            case R.id.copy_username_f:
                try {
                    set = decrypt(); //get the decrypted string
                } catch (Exception e) {
                    e.printStackTrace();
                }
                split = set.split("\\s+");//split the string
                ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE);//initialize the clipboard to gain access to the user's clipboard service
                ClipData clip = ClipData.newPlainText("Copied Username:", split[0]); //the clip data is the username string
                clipboard.setPrimaryClip(clip); //set the text in the clipboard
                copy_u.setText("COPIED!");//change the text shown on the button to inform the user that the string was copied
                Toast.makeText(getActivity(),"Copied your email!",Toast.LENGTH_LONG).show(); //toast message informs the user about this as well
                break;
            //in case the user wants to copy their password the appropriate sequence is executed
            case R.id.copy_password_f:
                try {
                    set = decrypt(); //get the decrypted string
                } catch (Exception e) {
                    e.printStackTrace();
                }
                split = set.split("\\s+"); //split the string
                clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE); //gain access to the user's clipboaes
                clip = ClipData.newPlainText("Copied Password:", split[1]); //the clip data is the password string
                clipboard.setPrimaryClip(clip); //set the text in the clipboard
                copy_p.setText("COPIED!");//change the text shown on the button to inform the user that the string was copied
                Toast.makeText(getActivity(),"Copied your Facebook password!",Toast.LENGTH_LONG).show(); // toast message informs the user about this as well
                break;

            //in case the user wants to delete their login details for this social media
            case R.id.delete_facebook:
                File dir = getActivity().getFilesDir(); //get the directory of the internal storage
                File file = new File(dir,"1.txt"); //get the directory of the file
                boolean deleted = file.delete(); //delete the file
                delete.setText("DELETED!"); //change the text shown on the button to inform the user that their details were deleted
                Toast.makeText(getActivity(),"Your Facebook login details were deleted successfully! ",Toast.LENGTH_LONG).show(); //toast message informs the user about this as well

                break;
        }
    }
}




