package com.example.fyp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This class is used to login the user in their personal account using Firebase
 * Firebase takes the appropriate actions to ensure that the user is who they say they are so after a few failed login attempts the account is blocked
 * The user here also has the ability to view their password hint and change their password if they forgot it
 */
public class LogIn extends AppCompatActivity {
    EditText u_email, u_password; //text box that contains the users login email and password
    TextView u_hint; //will contain the displayed hint
    Button loginbtn, forgotbtn, hintbtn; //three buttons displayed on this screen
    FirebaseAuth fAuth; //firebase authentication
    private static  final String FILE_NAME = "hint.txt"; //name of the file in which the hint is stored

    /**
     * Creates the content to be viewed on the users screen
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in); //load the appropriate layout
        //gets the views of each edittext box and buttons
        //alsop gets the instance of the firebase database
        u_email = findViewById(R.id.txt_email);
        u_password = findViewById(R.id.txt_password);
        fAuth = FirebaseAuth.getInstance();
        loginbtn = findViewById(R.id.b_login);
        forgotbtn = findViewById(R.id.bt_forgot);
        hintbtn = findViewById(R.id.b_hint);
        u_hint = findViewById(R.id.txt_hint);

        //if the user clicks on the login button
        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = u_email.getText().toString().trim();//gets the string stored in the email box
                final String pass = u_password.getText().toString().trim();//gets the string stored in the password box
                //if there is no entry for the email it displays an error message
                if (TextUtils.isEmpty(email)) {
                    u_email.setError("Required Field");
                    Toast.makeText(LogIn.this, "Required Field", Toast.LENGTH_SHORT).show();
                    return;
                }
                //if there is no entry for the password it displays an error message
                if (TextUtils.isEmpty(pass)) {
                    u_password.setError("Required Field");
                    Toast.makeText(LogIn.this, "Required Field", Toast.LENGTH_SHORT).show();
                    return;
                }
                //Login method is with the users email and password that is stored in firebase
                fAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @SuppressLint("ResourceType")
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //if the loging in task is successful
                        if (task.isSuccessful()) {
                            //if the users email got verified
                            if (fAuth.getCurrentUser().isEmailVerified()) {
                                Toast.makeText(LogIn.this, "User Logged In Succesfuly", Toast.LENGTH_SHORT).show(); //inform the user that the loging in was successful
                                setContentView(R.layout.activity_main);//load the appropriate activity
                                Intent intent = new Intent(getApplicationContext(), Interface.class); //load thr interface class
                                startActivity(intent); //start the interface class
                            } else {
                                Toast.makeText(LogIn.this, "Please Verify Your Email!", Toast.LENGTH_SHORT).show(); //if their email is not verified the user gets informed and is not logged in

                            }
                        } else {
                            Toast.makeText(LogIn.this, "Error" + task.getException().getMessage(), Toast.LENGTH_SHORT).show(); //if the log in fails then the user gets shown an error message and is not logged in
                        }
                    }
                });
            }
        });
        forgotbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = u_email.getText().toString().trim();
                FirebaseAuth.getInstance().sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            setContentView(R.layout.activity_log_in);
                            startActivity(new Intent(getApplicationContext(), LogIn.class));
                            Toast.makeText(LogIn.this, "Please check your email and reset your password", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
        hintbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FileInputStream fileInputStream = openFileInput(FILE_NAME);
                    int c;
                    String temp = "";
                    while ((c = fileInputStream.read()) != -1) {
                        temp = temp + Character.toString((char) c);
                    }
                    u_hint.setText(temp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            });
    }

    }

