package com.example.fyp;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


import com.google.android.material.navigation.NavigationView;

/**
 * The interface class implements methods to display the toolbar,navigation drawers and options in the drawers
 */
public class Interface extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //sets the content to be shown on the ap
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);  //declares and initialises the toolbar
        setSupportActionBar(toolbar);

        //declares and initialises the drawer that opens and closes the toolbar
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        //declares,initialises and displays the navigation view
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //add this line to display menu1 when the activity is loaded
        displaySelectedScreen(R.id.nav_passwords);

    }

    /**
     * Closes the drawer if the user presses the back button on their device
     */
    @Override
    public void onBackPressed() {
        //declare and initialize the drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START); // if drawer is open is open close it
        } else {
            super.onBackPressed(); //else nothing
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        //calling the method displayselectedscreen and passing the id of selected menu
        displaySelectedScreen(item.getItemId());
        //make this method blank
        return true;
    }

    /**
     * Uses a switch case to check which drawer was chosen and loads the appropriate fragment
     * @param itemId gets the id of the selected drawer menu item
     */
    private void displaySelectedScreen(int itemId) {

        //creating fragment object
        Fragment fragment = null;

        //initializing the fragment object which is selected
        switch (itemId) {
            //if the user selects the passwords the password fragment is loaded
            case R.id.nav_passwords:
                fragment = new Passwords_Fragment();
                break;
                //if the user selects the password generator it loads the password generator fragment
            case R.id.nav_generator:
                fragment = new password_generator_frag();
                break;
                //if the user selects the password strenght checker it loads the appropriate fragment
            case R.id.nav_strength:
                fragment = new password_strength_frag();
                break;
                //loads the help fragment
            case R.id.nav_help:
                fragment = new help();
                break;
                //loads the settings fragment
            case R.id.nav_settings:
                fragment = new settings();
                break;
                // if the user wants to logout the logout fragment gets loaded
            case R.id.nav_logout:
                fragment = new Logout_Fragment();
                break;
        }
        //replacing the fragment with the one the users wants to view
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.nav_host_fragment, fragment);
            ft.commit();
        }
        //then the drawer gets closed
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

}



