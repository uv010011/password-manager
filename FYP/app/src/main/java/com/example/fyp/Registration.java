package com.example.fyp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.SignInMethodQueryResult;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is used to register a new user to the application. The registration details are stored securely in firebase.
 * The user is asked to enter a valid email,password and password hint.
 * Each user can have only one account so this class makes sure that the user can't register with the same email.
 *
 */
public class Registration extends AppCompatActivity {
    EditText Email, password,hint; //declare boxes to be displayed
    Button registerbtn; //button to be displayed
    FirebaseAuth fAuth; //firebase authentication is declared
    Pattern pattern = Pattern.compile("[a-zA-Z0-9]*"); //pattern of the accepted password characters
    private static  final String FILE_NAME = "hint.txt"; //name of the file used to store the password hint

    /**
     * When the process starts it gets the user's entries and makes the appropriate checks to
     * ensure that the user is registered correctly and within the rules
     */
    @Override
    protected void onStart() {
        super.onStart();
        setContentView(R.layout.activity_registration); //set current layout
        //get IDs of the EditTexts and button
        Email = findViewById(R.id.Email);
        password = findViewById(R.id.password);
        hint = findViewById(R.id.passhint);
        registerbtn = findViewById(R.id.bt_reg);
        fAuth = FirebaseAuth.getInstance();//current instance of the firebase database

        //when the register button is clicked
        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final  String email = Email.getText().toString().trim();//get the email address
                final String pass= password.getText().toString().trim();//get the password
                final String u_hint = hint.getText().toString().trim();//get the hint
                //if the email entry is empty then display the error message
                if (TextUtils.isEmpty(email)) {
                    Email.setError("Required Field");//error message
                    Toast.makeText(Registration.this, "Required Field", Toast.LENGTH_SHORT).show();//error message as a toast
                    return;
                }
                //if th password entry is empty then display the error message
                if (TextUtils.isEmpty(pass)) {
                    password.setError("Required Field"); //error message
                    Toast.makeText(Registration.this, "Required Field", Toast.LENGTH_SHORT).show();//error message as a toast
                    return;
                }
                //if the hint entry is empty then display the error message
                if (TextUtils.isEmpty(u_hint)) {
                    hint.setError("Required Field");//errror message
                    Toast.makeText(Registration.this, "Required Field", Toast.LENGTH_SHORT).show();//error message as a toast
                    return;
                }
                //declare and initialize the pattern matcher
                Matcher matcher = pattern.matcher(pass);
                //if the password is less than 6 characters long and does not have the values shown in the pattern it shows an error message
                if (pass.length() < 6 || matcher.matches()) {
                    password.setError("Add 6 or more characters including numeric and special characters"); //error message
                    Toast.makeText(Registration.this, "Add 6 or more characters including numeric and special characters", Toast.LENGTH_SHORT).show();//error message as a toast
                    return;
                }
                //if the hint is the same as the password it displays an error
                if (u_hint.equals(pass) == true){
                    hint.setError("Your Hint Can't Be Your Password");//error message
                    Toast.makeText(Registration.this, "Your Hint Can't Be Your Password", Toast.LENGTH_SHORT).show();//error message as a toast
                    return;
                }
                final String finalPass = pass;
                //check if the given email is already registered
                checkEmail(email, new EmailCheckListener() {
                    @Override
                    public void onSuccess(boolean isReg) {
                        if (isReg) { //if it is
                            setContentView(R.layout.activity_log_in);//redirect user to log in page
                            startActivity(new Intent(getApplicationContext(), LogIn.class));
                            Toast.makeText(Registration.this, "Email is Already In Use Try Logging In", Toast.LENGTH_SHORT).show(); //inform user that the email is already in use
                        } else {
                            //if its a new email then it creates a new user with the entered email and password
                            fAuth.createUserWithEmailAndPassword(email, finalPass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    //if th task is successful
                                    if (task.isSuccessful()) {
                                        //send an email verification email
                                        fAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                //if sending the email verification was successfull
                                                if (task.isSuccessful()) {
                                                    try {
                                                        storeHint(v);//store the hint
                                                    } catch (FileNotFoundException e) {
                                                        e.printStackTrace(); //throws exception if the hint file is not found
                                                    }
                                                    Toast.makeText(Registration.this, "Account Created Succesfuly.Please check your email and verify your account.", Toast.LENGTH_SHORT).show(); //inform user about the creation of their account
                                                }
                                            }
                                        });

                                        setContentView(R.layout.activity_log_in);//redirect the user to the log in activity
                                        Intent intent = new Intent(getApplicationContext(), LogIn.class);//create a new intent with the login class
                                        startActivity(intent);//start the new activity
                                    } else {
                                        Toast.makeText(Registration.this, "Error" + task.getException().getMessage(), Toast.LENGTH_SHORT).show(); //in case of an error show this message
                                    }
                                }
                            });
                        }
                    }
                });

            }

        });

    }


    public interface EmailCheckListener {
        void onSuccess(boolean isReg);
    }

    public void checkEmail(final String email, final EmailCheckListener listener) {
        fAuth.fetchSignInMethodsForEmail(email).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
            @Override
            public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                boolean check = !task.getResult().getSignInMethods().isEmpty();
                listener.onSuccess(check);
            }
        });
    }

    private void storeHint(View v) throws FileNotFoundException {
        String u_hint = hint.getText().toString();

            FileOutputStream fos = null;
            try {
                fos = openFileOutput(FILE_NAME, MODE_PRIVATE);
                fos.write(u_hint.getBytes());
                fos.close();
                hint.getText().clear();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
