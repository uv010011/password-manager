package com.example.fyp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Random;

/**
 * This class informs the user about the strength of their password. If the password is weak the user is
 * asked to change it to a stronger one
 * Uses an on clickListener for the strength button
 */
public class password_strength_frag extends Fragment implements View.OnClickListener {
    EditText password; //EditText box displayed in this fragment
    TextView strength; //TextView box displayed in this fragment
    String pass;//string to get the password
    Button go;//button displayed in this fragment

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Method will inflate the layout and get the Ids of the objects in this view
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.password_strength_check, container, false);
        password = view.findViewById(R.id.enter_pass); //get the id of the EditText box containing the password
        strength = view.findViewById(R.id.strength);//get the id of the  TextView that displays the strength
        go = view.findViewById(R.id.strength_btn);//get the id of the button
        go.setOnClickListener(this);//Set an onClickListener to it
        return view;
    }

    /**
     * Save the current state of the view
     * and set the title of this activity
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Password Strength");
        calculate();

    }

    /**
     * Calculates the strength of the password depending on the length of the password
     */
    public void calculate(){
        pass = password.getText().toString(); //get the password
        //if its length is less than 6 then it is a weak password
        if (pass.length() <= 6) {
                strength.setText("WEAK PASSWORD: It is advised to change it for security purposes"); //inform user about weak password
            } else if (pass.length() == 8 ) { //if its length is less than 8 then it is a medium password
                strength.setText("MEDIUM: It is advised to change it for security purposes"); //inform the user about weak password
            } else if (pass.length() == 10) { //if the password has 10 characters then it is a strong password
                strength.setText("STRONG: The password is secure"); //inform the user about strong password
            } else if (pass.length() >= 12) {
                strength.setText("VERY STRONG: The password is very secure");//anything greater than 10 characters is considered a very strong password
            }
        }

    /**
     * When the button is clicked the method used to calculate the passwords strength is called
     * @param v
     */
    @Override
    public void onClick(View v) {
        calculate();
    }
}

